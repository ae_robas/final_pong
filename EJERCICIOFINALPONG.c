#include "raylib.h"
#include <stdio.h>
#include <stdlib.h>


typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, END } GameScreen;

int main()
{
   
    int screenWidth = 800;
    int screenHeight = 450;
	char windowTitle[30] = "Final PONG";
	    
    GameScreen screen = LOGO;  
	
	int framesCounter = 0;
	int timeCounter =99;
	
	Color titleColor = WHITE;
	titleColor.a = 0;
	
	Color logoColor = WHITE;
	logoColor.a = 0;
	
	Color Enter = RED;
	
	//pong
	
	const int velocidady = 15; 
    const int ballSize = 20;
    const int maxVelocity = 6; 
    const int minVelocity = 6;
    int dirpala = 0;    
	char *texto;
    Vector2 tamanoTexto;
    
    bool pause = false;
    
    int score1p = 0; 
    int score2p = 0;
   
    Rectangle paladerecha;
    
    paladerecha.width = 15;
    paladerecha.height = 90;    
    paladerecha.x = screenWidth - 50 - paladerecha.width;
    paladerecha.y = screenHeight/2 - paladerecha.height/2;
    
    
    Rectangle palaizquierda;
    
    palaizquierda.width = 15;
    palaizquierda.height = 90;    
    palaizquierda.x = 50;
    palaizquierda.y = screenHeight/2 - palaizquierda.height/2;
    
    
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
    
    Vector2 ballVelocity;  
    ballVelocity.x = minVelocity;
    ballVelocity.y = minVelocity;
	
	int iaLinex = screenWidth/2;

    InitWindow(screenWidth, screenHeight, windowTitle);
	
	Font fontTtf = LoadFontEx("resources/blocker.ttf", 32, 0, 250);
	
	//textures
	
	Texture2D mascotlogo = LoadTexture ("resources/mascotlogo.png");
	Texture2D finalpong = LoadTexture ("resources/finalpong.png");
	Texture2D pressenter = LoadTexture ("resources/pressenter.png");
	Texture2D win = LoadTexture ("resources/win.png");
	Texture2D empate = LoadTexture ("resources/empate.png");
	Texture2D exit = LoadTexture ("resources/exit.png");
	Texture2D gameover = LoadTexture ("resources/gameover.png");
	Texture2D playagain = LoadTexture ("resources/playagain.png");
	
	InitAudioDevice();      // Initialize audio device

    // Load sound
    Sound fxWav = LoadSound("resources/golpe.wav");
    Sound fxGoal = LoadSound("Resources/goal.wav");	
	Sound fxGooal = LoadSound("Resources/gooal.wav");
    
    SetTargetFPS(60);
    
    
  
    while (!WindowShouldClose())    
    {
     
        switch(screen)  
        {
            case LOGO:
            {
                
				framesCounter++; 
				
				if (logoColor.a < 255) logoColor.a++; 
				
				if (framesCounter > 300)  
				{
					screen = TITLE; 
					framesCounter = 0; 
				}

            } break; 
            case TITLE:  
            {
                framesCounter++;
                
				if (titleColor.a < 255) titleColor.a++;
				
				if (IsKeyPressed(KEY_ENTER)){
                    screen = GAMEPLAY;
                    framesCounter = 0;
                }
				score1p=0;
				score2p=0;

            } break;
            case GAMEPLAY: 
            { 
                
				framesCounter++;
				
				if(!pause)  
		    {
                 if (IsKeyDown(KEY_W)){ 
                 palaizquierda.y -= velocidady; 
            }
            
                 if (IsKeyDown(KEY_S)){
                 palaizquierda.y += velocidady;  
            }
            /*
                if (IsKeyDown(KEY_UP)){
                paladerecha.y -= velocidady;
            }
            
                if (IsKeyDown(KEY_DOWN)){
                paladerecha.y += velocidady;
              }*/
			  
			   if (IsKeyDown(KEY_RIGHT)){
                iaLinex += velocidady;
                if (iaLinex > (paladerecha.x - ballSize))
                    iaLinex = paladerecha.x - ballSize;
            }
            
              if (IsKeyDown(KEY_LEFT)){
                iaLinex -= velocidady;
                if (iaLinex < 0) iaLinex = 0;
            }
			  			  
            }
			
			  //IA
              
            if( ball.x > iaLinex){
                if (framesCounter % 20 == 0){
                    if(ball.y > paladerecha.y + (paladerecha.height/2)){
                        paladerecha.y += velocidady * GetRandomValue(0.5, 1.1);
                        dirpala = 1;
                    }
            
                    if(ball.y < paladerecha.y + (paladerecha.height/2)){
                        paladerecha.y -= velocidady * GetRandomValue(0.5, 1.1);
                        dirpala = -1;
                    }
                } else {
                    paladerecha.y += (velocidady - 7) * dirpala;
                }
            }
			
			//pause
			   if (IsKeyPressed(KEY_P)) 
		    {
                pause = !pause;
								
            }
			
					
			  if(palaizquierda.y<0){
              palaizquierda.y = 0;
            }
        
             if(palaizquierda.y > (screenHeight - palaizquierda.height)){
             palaizquierda.y = screenHeight - palaizquierda.height;
            }
        
             if(paladerecha.y<0){
             paladerecha.y = 0;
            } 
        
             if(paladerecha.y > (screenHeight - paladerecha.height)){
             paladerecha.y = screenHeight - paladerecha.height;
            }
        
             
            if(!pause){ 
              ball.x += ballVelocity.x; 
              ball.y += ballVelocity.y;  
            }
			
			 //Marcar Gol
             if(ball.x > screenWidth - ballSize){ 
			 
			 PlaySound(fxGoal);
          
             score1p++; 
			 score1p++;
			 score1p++;
			 score1p++;
			 score1p++;
			 score1p++;
			 
			
			 
             ball.y = screenHeight/2;
             ball.x = screenWidth/2;  
			
			
             ballVelocity.x = -minVelocity;
             ballVelocity.y = minVelocity;
            
            }else if(ball.x < ballSize){ 
			
			PlaySound(fxGooal);
			
             score2p++; 
			 score2p++;
			 score2p++;
			 score2p++;
			 score2p++;
			 score2p++;
             ball.x = screenWidth/2;
             ball.y = screenHeight/2;
             ballVelocity.x = minVelocity;
             ballVelocity.y = minVelocity;
            }
			
			if (score1p > 50){
			screen = END;}
			
			if (score2p > 50){
			screen = END;}
				
				
			 if((ball.y > screenHeight - ballSize) || (ball.y < ballSize) ){ 
             ballVelocity.y *=-1;
			 PlaySound(fxWav);
			 
            }
			
            if(CheckCollisionCircleRec(ball, ballSize, paladerecha)){ 
                if(ballVelocity.x>0){                
                    ballVelocity.x *=-1;
				    PlaySound(fxWav);
                }
            }
			
			if(CheckCollisionCircleRec(ball, ballSize, palaizquierda)){ 
                if(ballVelocity.x<0){                
                    ballVelocity.x *=-1;
					PlaySound(fxWav);
					}
            }		

            //contador

            if (!pause){
                if (framesCounter >= 60)
                {
                    timeCounter--;
                    framesCounter = 0;
                }
                    
                if (timeCounter < 0) screen = END;			
			}
				
            } break;
            case END: 
            {
				if (IsKeyPressed(KEY_ENTER))
				{
					screen = TITLE;
					timeCounter = 99;
				}
				
				if (IsKeyDown(KEY_ENTER)){ 
				 
                 screen = TITLE;  
            }

            } break;
            default: break;
        }
        
        
        
        BeginDrawing();
        
            ClearBackground(DARKBLUE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    DrawTexture(mascotlogo, 310 , 120, logoColor);
					
					//DrawText("ae", 280 ,100 , 200, logoColor);
                } break;
                case TITLE: 
                {
                    DrawTexture(finalpong, 180 , 120, titleColor);
					//DrawText("Final Pong", 150, 100, 100, titleColor);
					
					if ((framesCounter/30)%2) DrawTexture(pressenter, 300 , 300, WHITE);//DrawText("Press Enter", 300, 300, 30, RED);
					
                } break;
                case GAMEPLAY:
                { 
				          ClearBackground(DARKBLUE);
            
                 
                  DrawRectangleRec(paladerecha, WHITE);
            
                  
                  DrawRectangleRec(palaizquierda, WHITE);
            
               
                  DrawCircleV(ball, ballSize, WHITE);  
            
                
            
                  texto = FormatText("P 1");
            
                  DrawTextEx(fontTtf, texto, (Vector2){0,10}, 40, 0, WHITE);

                  texto = FormatText("P 2");
                  tamanoTexto = MeasureTextEx(fontTtf, texto, 40, 0); 
            
                  DrawTextEx(fontTtf, texto, (Vector2){screenWidth - tamanoTexto.x,10}, 40, 0, WHITE);
                  //DrawText(FormatText("SCORE 1P: %d",score1p), 10, 10, 40, RED); 
                  //DrawText(FormatText("SCORE 2P: %d",score2p), screenWidth - 300, 10, 40, RED);
            
                  if(pause){
                   DrawRectangle(0, 0, screenWidth, screenHeight, (Color){ 255, 255, 255, 255/2 });  
                   DrawText("Press p to continue", screenWidth/2 - MeasureText("Press p to continue", 40)/2 , screenHeight/2, 40, RED); 
                }
                    
			    //contador
				
				DrawText(FormatText("%i", timeCounter), 370, 20, 40, WHITE);
				
				//barra vida P 1
				
				 DrawRectangle(80, 17, 50, 25, WHITE);
                 DrawRectangle(80, 17,(int)score2p, 25, RED);
				 
				 //barra vida P 2
				 
				 DrawRectangle(680, 17, 50, 25, WHITE);
                 DrawRectangle(680, 17,(int)score1p, 25, RED);
				 
				 
				
                } break;
                case END: 
                {
					
					if (score1p > score2p){
						DrawTexture(win, 200 , 100, WHITE);//DrawText("You Win", 190, 100, 100, WHITE);
					    DrawTexture(playagain, 270 , 260, WHITE);//DrawText("Press ENTER to play again", 270, 260, 20, RED);
					    DrawTexture(exit, 220 , 350, WHITE);//DrawText("Press ESC to exit", 270, 300, 20, RED);
					
				    }   
								
				
				    if (score2p > score1p){
						DrawTexture(gameover, 150 , 100, WHITE);//DrawText("Game Over", 150, 100, 100, WHITE);
					    DrawTexture(playagain, 270 , 260, WHITE);//DrawText("Press ENTER to play again", 270, 260, 20, RED);
					    DrawTexture(exit, 220 , 350, WHITE);//DrawText("Press ESC to exit", 270, 300, 20, RED);
					}
					
					  if (score2p == score1p){
						DrawTexture(empate, 150 , 100, WHITE);//DrawText("None Wins", 150, 100, 100, WHITE);
					    DrawTexture(playagain, 270 , 260, WHITE);//DrawText("Press ENTER to play again", 270, 260, 20, RED);
					    DrawTexture(exit, 200 , 350, WHITE);//DrawText("Press ESC to exit", 270, 300, 20, RED);
					}
                   
                           
                } break;
                default: break;
            }
        
            //DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
	UnloadSound(fxWav);     // Unload sound data

    CloseAudioDevice();
	
   // Unload sound data

  

    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}